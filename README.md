# branding

This repository contains the branding, artwork, and music for Sapphire Linux. Licensed under CC-BY-SA 4.0, you may use and reshare this media as long as you:

1. Give credit back to the Sapphire Developers
2. Share under the same License as you retrieved it.
3. Abide by the rest of the legal terms of the CC-BY-SA 4.0 License.

This is a mere summary... read the LICENSE file for the full legalese.
